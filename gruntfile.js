module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        mochacli: {
            test: {
                src: ['src/apps/calculator/tests/mocha-inject-phantom.js'],
                options: {
                    log: true,
                    run: true,
                    reporter: 'Nyan'
                },
                ui: 'bdd'
            }
        }
    });
    grunt.loadNpmTasks('grunt-mocha-cli');
    grunt.registerTask('default', ['mochacli']);
};