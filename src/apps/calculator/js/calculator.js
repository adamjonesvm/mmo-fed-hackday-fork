var VM = (function (vm) {
	"use strict";

	vm.calculator = (function () {
		var getIntById = function (id) {
				return parseInt(document.getElementById(id).value, 10);
			},
			calculate = function () {
				var sum = getIntById('x') + getIntById('y');
				document.getElementById('result').innerHTML = isNaN(sum) ? 0 : sum;
			},
			init = function () {
				document.getElementById('add').addEventListener('click', calculate);
			};

		return {
			init: init
		};

	}());

	return vm;

}(VM || {}));

(function () {
	"use strict";
	VM.calculator.init();
})();