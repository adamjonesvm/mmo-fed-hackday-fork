This is a project to be used on at the MMO FED SIP Hackdays

## Clone Project ##

* Open up a Terminal Window

* **cd** to your chosen location to download the project to

* Clone the Project to your local machine: **git clone https://bitbucket.org/adamjonesvm/mmo-fed-hackday-fork.git**

## Install ##

* **cd mmo-fed-hackday-fork** ( the project folder )

* **npm install**

## This Fork ##

This setup aims to:
	- use the DOM from the source folder to remove the need to copy any elements into a separate test.html file.
	- stop the need for copying blocks of DOM elements to inject per test
	- call phantom with source DOM in each test and teardown after to make each test fair and isolated.
	- fun a single command from grunt to initiate all tests
	- no need to expose private methods

In grunt simply call 
* **grunt mochacli**

NOTE: may need to globally install modules (grunt-cli etc.) / setup bower to avoid issues if any of the modules are missing.

## Cleaner ##

If you know of or find a cleaner way to implement this, let me know.